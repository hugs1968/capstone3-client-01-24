import {Bar} from 'react-chartjs-2'

export default function BartChartExpense(){
	return(

			<Bar data = {{

				datasets:[{

					data: [],//array of numbers
					backgroundColor: ["lightblue","black","green","yellow"]
				
				}],
				labels: [
					
					'January',
					'February',
					'March',
					'April',
					'May',
					'June',
					'July',
					'August',
					'September',
					'October',
					'November',
					'December'

				]

			}} />

		)

}