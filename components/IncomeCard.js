import {useState, useEffect} from 'react'
import {Row,Col} from 'react-bootstrap'
import Card from 'react-bootstrap/Card'
import {Fragment} from 'react'

export default function IncomeCard({prop, date}){

	const {_id, description, categoryType, categoryName, amount} = prop

	return(
		<Fragment>
			<div className="py-2">
			<Card>
			<Card.Body>
				<Row>
					<Col md={6}>
					<h5>{description}</h5>									

					{
						categoryType === "Expense"
						?
						<h6><span className="text-danger">Expense</span> ({categoryName})</h6>
						:
						(categoryType === "Income")                   
	                    ?
	                    <h6><span className="text-success">Income</span> ({categoryName})</h6>
	                    :
	                    null

					}
					
					<p>{date}</p>
					</Col>
					<Col md={6} className="text-right">
					{
						categoryType === "Expense"
						?
						<h6 className="text-danger">- {amount}</h6>
						:
						(categoryType === "Income")                    
	                    ?
	                    <h6 className="text-success">+ {amount}</h6>
	                    :
	                    null

					}			
					</Col>
				</Row>
			</Card.Body>
			</Card>
			</div>
		</Fragment>


		)

}