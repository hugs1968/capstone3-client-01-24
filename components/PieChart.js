import {Pie} from 'react-chartjs-2'

export default function PieBreakdown(){
	return(

			<Pie data = {{

				labels: ['Allowance','Bills','Salary'],
				datasets:[{

					data: [1,2,3],//array of numbers
					backgroundColor: ["green","lightblue","red"]
				
				}]

			}} />

		)

}
