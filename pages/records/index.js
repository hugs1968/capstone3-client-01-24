import {useState,useEffect,useContext} from 'react'
import Record from '../../components/RecordsCard'
import AppHelper from '../../app_helper.js'
import UserContext from '../../UserContext'
import {Fragment} from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import moment from 'moment'
import Card from 'react-bootstrap/Card'
import {Row,Col} from 'react-bootstrap'

export default function Records() {

  const {user} = useContext(UserContext)
  const [records, setRecords] = useState([])
  const [showOptions, setShowOptions] = useState('')
  const [incomeCategory,setIncomeCategory] = useState([])
  const [expenseCategory,setExpenseCategory] = useState([])


  useEffect(() => {
    
  const incomeCard = []
  const expenseCard = []

  const option = {
      headers: {
        Authorization: `Bearer ${AppHelper.getAccessToken()}`
      }
    }

    fetch(`${AppHelper.API_URL}/users/records`, option)
    .then(AppHelper.toJSON)
    .then(data => {
      console.log(data)    
     

      const allRecords = data.map(record => {

        const date = moment(record.dateMade).format('MMMM DD YYYY')

        if(record.categoryType === "Income"){

          incomeCard.push(record) //put all records in an array         

        } else if (record.categoryType === "Expense") {

          expenseCard.push(record)          

        }
        
        console.log(incomeCard)
        console.log(expenseCard)

        return <Record key={record._id} prop={record} date={date}/>

      })
      setRecords(allRecords)
      setIncomeCategory(

                      incomeCard.map(income => (

                        

                          <div className="py-2">
                            <Card>
                            <Card.Body>
                              <Row>
                                <Col md={6}>
                                <h5>{income.description}</h5>  
                                <h6><span className="text-success">Income</span> ({income.categoryName})</h6>                                 
                                
                                </Col>
                                <Col md={6} className="text-right">
                                <h6 className="text-success">+ {income.amount}</h6>  
                                </Col>
                              </Row>
                            </Card.Body>
                            </Card>
                            </div>

                        ))

                      )


       setExpenseCategory(

                      expenseCard.map(expense => (

                        

                          <div className="py-2">
                            <Card>
                            <Card.Body>
                              <Row>
                                <Col md={6}>
                                <h5>{expense.description}</h5>  
                                <h6><span className="text-danger">Expense</span> ({expense.categoryName})</h6>                                 
                                
                                </Col>
                                <Col md={6} className="text-right">
                                <h6 className="text-danger">+ {expense.amount}</h6>  
                                </Col>
                              </Row>
                            </Card.Body>
                            </Card>
                            </div>

                        ))

                      )   

    })

    

   
    
  },[])

  function search(e) {



  }



  return (

    <>
      <div className="pt-3 mb-5 container">
      <h3>Records</h3>
      <Form onSubmit={(e)=> showRecords(e)}>    
        
        <Form.Group>
        <div className="mb-2 input-group">
          <Button href="/records/new" className="btn btn-success" >Add</Button>
          <Form.Control type="text" placeholder="Search Record" className="form-control" />
          <select value={showOptions} onChange={e => setShowOptions(e.target.value)} className="form-control">
            <option value="All" selected>All</option>
            <option value="Income">Income</option>
            <option value="Expense">Expense</option>
           
          </select>
        </div>
        </Form.Group>
        
      </Form>

      <hr />
      {

        showOptions === "All"
        ?
        records
          : 
          (showOptions === "Income")                   
          ?
          incomeCategory
            :
            (showOptions === "Expense")
            ?
            expenseCategory
            :
            null   

      }
      
      </div>
    </>
    )
}

/*condition1 
        ? 
        value1
         : 
         condition2 
         ? 
         value2
         : 
         condition3 
         ? 
         value3
         : 
         value4;*/