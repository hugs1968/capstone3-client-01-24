import {useEffect} from 'react'
import BarChartIncome from './../../components/BarChart'

export default function MonthlyIncome(){
	return(
		<>
			<h2 className="text-center">Monthly Income</h2>
			<BarChartIncome />
		</>
		)
}