import {Fragment,useState} from 'react'
import PieBreakdown from './../../components/PieChart'
import {Row,Col} from 'react-bootstrap'
import {Container} from 'react-bootstrap'
import Form from 'react-bootstrap/Form'

export default function CategoryBreakdown(){

	const [dateFrom,setDateFrom] = useState('')
	const [dateTo,setDateTo] = useState('')

	return(
			<div className="mt-5 pt-4 mb-5">
			<Container>
			<Fragment>
			<h2 className="text-center">Category Breakdown</h2>

		
			<Form>
				<Row>
					<Col>				
						<Form.Group>
							<Form.Label>From</Form.Label>
							<Form.Control type="date" className="form-control" value={dateFrom} placeholder="2020-12-22" />
						</Form.Group>
					</Col>
					<Col>
						<Form.Group>
							<Form.Label>To</Form.Label>
							<Form.Control type="date" className="form-control" value={dateTo} placeholder="2021-01-22" />
						</Form.Group>
					</Col>				
				</Row>
			</Form>			
			<hr />
			<PieBreakdown />

			</Fragment>
			</Container>
			</div>
		)
}