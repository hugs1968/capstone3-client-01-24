import BarChartExpense from './../../components/BarChart2'

export default function MonthlyExpense(){
	return(
		<>
			<h2 className="text-center">Monthly Expense</h2>
			<BarChartExpense />
		</>
		)
}